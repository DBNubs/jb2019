import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PostsService {

  constructor(private http: Http) { }

  getAllPosts() {
    return this.http.get('http://api.jasonblanda.com/api/posts')
      .map(res => res.json());
  }

  getAllWebDev() {
    return this.http.get('http://api.jasonblanda.com/api/webdev')
      .map(res => res.json());
  }

  getAllRants() {
    return this.http.get('http://api.jasonblanda.com/api/rants')
      .map(res => res.json());
  }

  getFeaturedWork() {
    return this.http.get('http://api.jasonblanda.com/api/portfolio/featured')
      .map(res => res.json());
  }

  getAllWork() {
    return this.http.get('http://api.jasonblanda.com/api/portfolio')
      .map(res => res.json());
  }

  getSelectedPost(id: string) {
    return this.http.get('http://api.jasonblanda.com/api/node/' + id)
      .map(res => res.json());
      
  }



}
