import { Component, OnInit } from '@angular/core';

import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-rants',
  templateUrl: './rants.component.html',
  styleUrls: ['./rants.component.scss']
})
export class RantsComponent implements OnInit {

  posts: any = [];

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.postsService.getAllRants().subscribe(posts => {
      this.posts = posts;
    })
  }

}
