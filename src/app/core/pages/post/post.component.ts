import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  post: any = [];
  id: string;

  constructor(private postsService: PostsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.route.params.subscribe( params =>
        this.id = params['id']
    )

    this.postsService.getSelectedPost(this.id).subscribe(post => {
      this.post = post;
    });

  }

}
