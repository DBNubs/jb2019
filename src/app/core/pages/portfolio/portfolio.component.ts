import { Component, OnInit } from '@angular/core';

import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  items: any = [];
  count;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.postsService.getAllWork().subscribe(posts => {
      this.items = posts;
    }, (error) => {
      console.log(error);
    }, () => {
      this.count = this.items.length;
    })
  }

}
