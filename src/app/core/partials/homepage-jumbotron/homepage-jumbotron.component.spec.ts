import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageJumbotronComponent } from './homepage-jumbotron.component';

describe('HomepageJumbotronComponent', () => {
  let component: HomepageJumbotronComponent;
  let fixture: ComponentFixture<HomepageJumbotronComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepageJumbotronComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageJumbotronComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
