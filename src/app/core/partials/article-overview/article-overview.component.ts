import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-article-overview',
  inputs: ['post'],
  templateUrl: './article-overview.component.html',
  styleUrls: ['./article-overview.component.scss']
})
export class ArticleOverviewComponent implements OnInit {
  post;
  
  constructor() { }

  ngOnInit() {
  }

}
