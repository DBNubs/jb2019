import { Component, OnInit } from '@angular/core';

import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-featured-work',
  templateUrl: './featured-work.component.html',
  styleUrls: ['./featured-work.component.scss']
})
export class FeaturedWorkComponent implements OnInit {

  items: any = [];

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.postsService.getFeaturedWork().subscribe(items => {
      this.items = items;
    })
  }

}
