import { Component, OnInit } from '@angular/core';

import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-homepage-articles',
  templateUrl: './homepage-articles.component.html',
  styleUrls: ['./homepage-articles.component.scss']
})
export class HomepageArticlesComponent implements OnInit {

  posts: any = [];

  constructor(private postsService: PostsService) { }

  ngOnInit() {

    this.postsService.getAllPosts().subscribe(posts => {
      this.posts = posts;
    })
  }

}
