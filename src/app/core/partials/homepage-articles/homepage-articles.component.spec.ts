import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageArticlesComponent } from './homepage-articles.component';

describe('HomepageArticlesComponent', () => {
  let component: HomepageArticlesComponent;
  let fixture: ComponentFixture<HomepageArticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepageArticlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
