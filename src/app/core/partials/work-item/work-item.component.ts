import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-work-item',
  inputs: ['item'],
  templateUrl: './work-item.component.html',
  styleUrls: ['./work-item.component.scss']
})
export class WorkItemComponent implements OnInit {
  item;

  constructor() { }

  ngOnInit() {
  }

}
