import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { NavigationComponent } from './core/partials/navigation/navigation.component';
import { HomepageJumbotronComponent } from './core/partials/homepage-jumbotron/homepage-jumbotron.component';
import { HomepageAboutComponent } from './core/partials/homepage-about/homepage-about.component';
import { HomepageComponent } from './core/pages/homepage/homepage.component';
import { ArticleOverviewComponent } from './core/partials/article-overview/article-overview.component';
import { HomepageArticlesComponent } from './core/partials/homepage-articles/homepage-articles.component';
import { PostsService } from './core/services/posts.service';
import { FeaturedWorkComponent } from './core/partials/featured-work/featured-work.component';
import { WorkItemComponent } from './core/partials/work-item/work-item.component';
import { WebdevComponent } from './core/pages/webdev/webdev.component';
import { RantsComponent } from './core/pages/rants/rants.component';
import { PortfolioComponent } from './core/pages/portfolio/portfolio.component';
import { ContactComponent } from './core/pages/contact/contact.component';
import { PostComponent } from './core/pages/post/post.component';

const appRoutes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'web-dev', component: WebdevComponent},
  {path: 'rants', component: RantsComponent},
  {path: 'portfolio', component: PortfolioComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'web-dev/:id', component: PostComponent},
  {path: 'rants/:id', component: PostComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomepageJumbotronComponent,
    HomepageAboutComponent,
    HomepageComponent,
    ArticleOverviewComponent,
    HomepageArticlesComponent,
    FeaturedWorkComponent,
    WorkItemComponent,
    WebdevComponent,
    RantsComponent,
    PortfolioComponent,
    ContactComponent,
    PostComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true}
    )
  ],
  providers: [PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
